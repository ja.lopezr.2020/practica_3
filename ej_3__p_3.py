#!/usr/bin/python3

import csv
import os.path
import urllib.request

#import hamcrest
#hamcrest.is_(param1, param2)

#from hamcrest import is_
#is_(param1, param2)

#from hamcrest import is_

url = 'https://corgis-edu.github.io/corgis/datasets/csv/airlines/airlines.csv'

if not os.path.exists("airlines.csv"):
    urllib.request.urlretrieve(url, filename = "airlines.csv")

with open ('airlines.csv') as info:
    datos = csv.reader(info, delimiter = ",")

    codigo_ap = []
    meses = []
    cancelados = []
    total_airlines = {}

    is_header = True

    for dato in datos:
        if not is_header:
            codigo_ap.append(dato[0])
            if dato[2] not in meses:
                meses.append(dato[2])

            aeropuerto = dato[0]
            num_compañias = dato[12]
            if aeropuerto in total_airlines:
                total_airlines[aeropuerto].append(int(num_compañias))
            else:
                total_airlines[aeropuerto] = [int(num_compañias)]
        else:
            is_header = False

    is_header = True

    for dato in datos:
        if not is_header:
            aeropuerto = dato[0]
            porc_cancelados = int(dato[13])/int(dato[17]) * 100
            cancelados.append(porc_cancelados)
        if dato[2] not in meses:
                meses.append(dato[2])
        else:
            is_header = False

# print(cancelados)
print("El numero de aerolineas para cada aeropuerto fue:")
for aeropuerto in total_airlines:
    lista_1 = total_airlines[aeropuerto]
    media = sum(lista_1)/len(lista_1)
    print(aeropuerto, "la media de aerolineas que han operado es:", media)

print("El peor mes en porcentaje respecto a vuelos cancelados para cada aeropuerto fue:")
for aeropuerto in cancelados:
    lista_2 = cancelados[int(aeropuerto)]
    maximo = max(lista_2)
    posicion = lista_2.index(maximo) # <-- me da el índice del maximo
    mes = meses[posicion]
    print(aeropuerto, mes, maximo)